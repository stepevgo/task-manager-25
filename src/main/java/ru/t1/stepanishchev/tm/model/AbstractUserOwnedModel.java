package ru.t1.stepanishchev.tm.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

public abstract class AbstractUserOwnedModel extends AbstractModel {

    @Nullable
    @Getter
    @Setter
    private String userId;

}