package ru.t1.stepanishchev.tm.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import java.util.UUID;

public abstract class AbstractModel {

    @NotNull
    @Getter
    @Setter
    private String id = UUID.randomUUID().toString();

}