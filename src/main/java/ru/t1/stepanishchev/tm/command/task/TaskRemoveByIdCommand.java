package ru.t1.stepanishchev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.stepanishchev.tm.util.TerminalUtil;

public final class TaskRemoveByIdCommand extends AbstractTaskCommand {

    @NotNull
    private final String NAME = "task-remove-by-id";

    @NotNull
    private final String DESCRIPTION = "Remove task by id.";

    @Override
    public void execute() {
        System.out.println("[REMOVE TASK BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final String userId = getUserId();
        getTaskService().removeOneById(userId, id);
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

}