package ru.t1.stepanishchev.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.stepanishchev.tm.command.AbstractCommand;

import java.util.Collection;

public final class ArgumentListCommand extends AbstractSystemCommand {

    @NotNull
    private final String NAME = "arguments";

    @NotNull
    private final String ARGUMENT = "-arg";

    @NotNull
    private final String DESCRIPTION = "Show list of arguments.";

    @Override
    public void execute() {
        System.out.println("[ARGUMENTS]");
        @NotNull final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        for (final AbstractCommand command : commands) {
            @Nullable final String argument = command.getArgument();
            if (argument == null || argument.isEmpty()) continue;
            System.out.println(argument);
        }
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

}