package ru.t1.stepanishchev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.stepanishchev.tm.enumerated.Status;
import ru.t1.stepanishchev.tm.util.TerminalUtil;

public final class ProjectStartByIdCommand extends AbstractProjectCommand {

    @NotNull
    private final String NAME = "project-start-by-id";

    @NotNull
    private final String DESCRIPTION = "Start project by id.";

    @Override
    public void execute() {
        System.out.println("[START PROJECT BY ID]");
        System.out.println("[ENTER ID: ]");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final String userId = getUserId();
        getProjectService().changeProjectStatusById(userId, id, Status.IN_PROGRESS);
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

}