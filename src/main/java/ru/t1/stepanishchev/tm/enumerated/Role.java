package ru.t1.stepanishchev.tm.enumerated;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;

public enum Role {

    USUAL("Usual user"),
    ADMIN("Administrator");

    @NotNull
    @Getter
    private final String displayName;

    Role(@NotNull String displayName) {
        this.displayName = displayName;
    }

}