package ru.t1.stepanishchev.tm.enumerated;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.stepanishchev.tm.comparator.CreatedComparator;
import ru.t1.stepanishchev.tm.comparator.NameComparator;
import ru.t1.stepanishchev.tm.comparator.StatusComparator;
import ru.t1.stepanishchev.tm.model.Task;

import java.util.Comparator;

public enum TaskSort {

    BY_NAME("Sort by name", NameComparator.INSTANCE::compare),
    BY_STATUS("Sort by status", StatusComparator.INSTANCE::compare),
    BY_CREATED("Sort by created", CreatedComparator.INSTANCE::compare),
    BY_DEFAULT("Without sort", null);

    @NotNull
    @Getter
    private final String name;

    @Nullable
    @Getter
    private final Comparator<Task> comparator;

    @NotNull
    public static TaskSort toSort(@Nullable final String value) {
        if (value == null || value.isEmpty()) return BY_DEFAULT;
        for (@NotNull final TaskSort sort : values()) {
            if (sort.name().equals(value)) return sort;
        }
        return BY_DEFAULT;
    }

    TaskSort(@NotNull final String displayName, @Nullable final Comparator<Task> comparator) {
        this.name = displayName;
        this.comparator = comparator;
    }

}